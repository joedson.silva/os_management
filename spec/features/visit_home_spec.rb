require 'rails_helper'

feature 'Guest access service page' do
    scenario 'has success' do
        visit services_path

        expect(page).to have_content('Services')
    end

    scenario 'add new service' do
        visit new_service_path
        fill_in 'Description', with: 'Serviço teste (auto)'
        fill_in 'Value', with: 89.99
        # select "option_name_here", :from => "organizationSelect"
        click_on 'commit'

        expect(page).to have_content('Service was successfully created.')
    end
end