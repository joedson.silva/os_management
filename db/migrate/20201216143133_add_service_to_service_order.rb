class AddServiceToServiceOrder < ActiveRecord::Migration[6.0]
  def change
    add_reference :service_orders, :service, null: false, foreign_key: true
  end
end
