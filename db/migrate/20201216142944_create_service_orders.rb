class CreateServiceOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :service_orders do |t|
      t.string :employee
      t.string :detail
      t.date :date
      t.datetime :start
      t.datetime :end
      t.integer :amount

      t.timestamps
    end
  end
end
