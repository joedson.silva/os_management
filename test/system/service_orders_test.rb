require "application_system_test_case"

class ServiceOrdersTest < ApplicationSystemTestCase
  setup do
    @service_order = service_orders(:one)
  end

  test "visiting the index" do
    visit service_orders_url
    assert_selector "h1", text: "Service Orders"
  end

  test "creating a Service order" do
    visit service_orders_url
    click_on "New Service Order"

    fill_in "Amount", with: @service_order.amount
    fill_in "Date", with: @service_order.date
    fill_in "Detail", with: @service_order.detail
    fill_in "Employee", with: @service_order.employee
    fill_in "End", with: @service_order.end
    fill_in "Start", with: @service_order.start
    click_on "Create Service order"

    assert_text "Service order was successfully created"
    click_on "Back"
  end

  test "updating a Service order" do
    visit service_orders_url
    click_on "Edit", match: :first

    fill_in "Amount", with: @service_order.amount
    fill_in "Date", with: @service_order.date
    fill_in "Detail", with: @service_order.detail
    fill_in "Employee", with: @service_order.employee
    fill_in "End", with: @service_order.end
    fill_in "Start", with: @service_order.start
    click_on "Update Service order"

    assert_text "Service order was successfully updated"
    click_on "Back"
  end

  test "destroying a Service order" do
    visit service_orders_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Service order was successfully destroyed"
  end
end
