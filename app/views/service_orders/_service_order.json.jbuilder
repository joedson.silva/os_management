json.extract! service_order, :id, :employee, :detail, :date, :start, :end, :amount, :created_at, :updated_at
json.url service_order_url(service_order, format: :json)
