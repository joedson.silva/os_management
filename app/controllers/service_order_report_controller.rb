class ServiceOrderReportController < ApplicationController
    def index
        @service_orders = []
        @total_sum = 0
        @final_total_sum = 0

        ServiceOrder.all.each do |os|    
            discount = 0
            
            if os.amount >= 10 && os.amount <= 30
                discount = 10 * (os.amount / 10).ceil
            end

            @total_sum += os.amount * os.service.value
            @final_total_sum += os.amount * os.service.value - (((os.amount * os.service.value)/100) * discount)

            @service_orders.push({
                "service": os.service.description,
                "value": os.service.value,
                "amount": os.amount,
                "total": os.amount * os.service.value,
                "discount": discount,
                "final_total": os.amount * os.service.value - (((os.amount * os.service.value)/100) * discount)
           })
        end
    end
end
