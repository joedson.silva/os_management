Rails.application.routes.draw do
  resources :service_orders
  resources :services

  get "/service_order_report" => "service_order_report#index"
  
end
